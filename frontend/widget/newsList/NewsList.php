<?php

namespace frontend\widget\newsList;

use yii\base\Widget;
use frontend\models\Test;
use Yii;

class NewsList extends Widget {

    public $showLimit = null;
    const NEWS_LIST_3 = 3;
    const NEWS_LIST_5 = 5;

    public function run() {

        $max = Yii::$app->params['maxNewsInList'];
        
        if($this->showLimit){
            $max = $this->showLimit;
        }
        
        $list = Test::getNewsList($max);
        
        return $this->render('block', [
                    'list' => $list
        ]);
    }

}
