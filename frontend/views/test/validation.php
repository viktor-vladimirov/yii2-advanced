<?php
/* @var $model frontend\models\Employee */

if ($model->hasErrors()) {
    echo '<pre>';
    print_r($model->getErrors());
    echo '<pre>';
}
?>
<h1>Запил Окон!</h1>

<form method="post">
    <p>Ширина (от 70 до 210 см):</p>
    <input type="text" name="width" />
    <br><br>

    <p>Высота (от 100 до 200 см):</p>
    <input type="text" name="size" />
    <br><br>

    <p>Количество камер:</p>
    <p><input type="radio" name="camera" value="1"> 1 </p>
    <br>

    <p><input type="radio" name="camera" value="2"> 2 </p>
    <br>

    <p><input type="radio" name="camera" value="3"> 3 </p>
    <br><br>

    <p>Общее количество створок:</p>
    <input type="text" name="flap" />
    <br><br>

    <p>Количество поворотных створок:</p>
    <input type="text" name="flapTurning" />
    <br><br>
    
    <p>Email:</p>
    <input type="text" name="email" />
    <br><br>

    <input type="submit" />
</form>
