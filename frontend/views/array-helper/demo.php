<?php
/* @var $employee array */
use yii\helpers\ArrayHelper;
use yii\helpers\Html;


$emails = ArrayHelper::getColumn($employee, 'email');

echo implode(', ', $emails);

echo '<hr>';

$array = [
    '1' => 'Bierut',
    '2' => 'Berlin',
    '3' => 'Baku',
    '4' => 'Rome',
];

$listData = ArrayHelper::map($employee, 'firstName', 'email');

echo '<pre>';
print_r($listData);
echo '<pre>';

echo Html::dropDownList('emails', [], $listData);