<?php
    /* @var $model frontend\models\Subscribe */
    /*@var $this yii\web\View */

/*
if(Yii::$app->session->hasFlash('subscriberStatus')){
    echo Yii::$app->session->getFlash('subscriberStatus');
}
 * 
 */

use frontend\assets\GalleryAsset;
GalleryAsset::register($this);

$this->title = 'Подпишитесь на новости';
$this->registerMetaTag([
    'name' => 'Description',
    'content' => 'Description of the page...',
]);
$this->params['breadcrumbs'] = [
    ['label' => 'About', 'url' => ['/site/about']],
    ['label' => 'Gallery', 'url' => ['/gallery']],
    'Test3',
];

if($model->hasErrors()){
    echo '<pre>';
    print_r($model->getErrors());
    echo '<pre>';
}
?>

<form method="post">
    <p>Email:</p>
    <input type="text" name="email" />
    <br><br>
    <input type="submit" />
</form>