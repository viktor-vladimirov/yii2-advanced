<?php

use yii\helpers\Html;

echo HTML::tag('h1', 'some text');

$array = [
    '1' => 'Bierut',
    '2' => 'Berlin',
    '3' => 'Baku',
    '4' => 'Rome',
];

echo Html::dropDownList('city id', [], $array);

echo Html::radioList('city id', [], $array);

echo Html::checkboxList('city id', [], $array);

echo Html::img('@images/burger.jpg', ['alt' => 'Burger']);