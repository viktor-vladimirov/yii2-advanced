<?php
/* @var $this yii\web\View */
/* @var $book frontend\models\Book */
/* @var $publishers array */

use yii\helpers\Html;
use yii\widgets\ActiveForm;


?>

<?php $form = ActiveForm::begin(); ?>

<?php echo $form->field($book, 'name'); ?>

<?php echo $form->field($book, 'isbn'); ?>

<?php echo $form->field($book, 'date_published'); ?>

<?php echo $form->field($book, 'publisher_id')->dropDownList($publishers); ?>

<?php echo Html::submitButton('Send', [
    'class' => 'btn btn-primary',
]) ;?>

<?php ActiveForm::end(); ?>