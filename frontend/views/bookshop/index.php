<?php
/* @var $this yii\web\View */
/* @var $bookList[] frontend\models\Book */

use yii\helpers\Url;

?>
<h1>Books!</h1>

<a href="<?php echo Url::to(['create']); ?>" class="btn btn-primary">Create book</a>
<br><br>

<?php foreach ($bookList as $book): ?>
    <div class="col-md-10">
        <h3><?php echo $book->name; ?></h3>
        <h3><?php echo $book->getDatePublished(); ?></h3>
        <?php $book->getPublisherName() ?>
        <p><?php $book->getFullName() ?></p>
        <hr>
    </div>
<?php endforeach; ?>
