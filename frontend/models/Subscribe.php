<?php

namespace frontend\models;

use yii\base\Model;
use Yii;

class Subscribe extends Model {

    public $email;
    public $birth;
    public $startDate;
    public $city;
    public $position;
    public $id;



    public function rules() {

        return [
            [['email'], 'required'],
            [['email'], 'email'],
            
            /*
            [['email', 'startDate', 'position' , 'id'], 'required'],
            [['email'], 'email'],                        
            ['birth', 'match', 'pattern' => '/[0-9]{4}-[0-9]{2}-[0-9]{2}/'],
            ['startDate', 'match', 'pattern' => '/[0-9]{4}-[0-9]{2}-[0-9]{2}/'],
            [['position'],'string'],
            [['id'], 'string', 'max' => 10],
            */
        ];
    }

    public function save() {

        $sql = "INSERT INTO subscriber (id , email) VALUE (null, '{$this->email}')";
        return Yii::$app->db->createCommand($sql)->execute();
    }

}
