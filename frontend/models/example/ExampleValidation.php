<?php

namespace frontend\models\example;

use Yii;

class ExampleValidation extends \yii\base\Model {

    const QUANTITY_1 = 1;
    const QUANTITY_2 = 2;
    const QUANTITY_3 = 3;

    public $width;
    public $size;
    public $camera;
    public $flap;
    public $flapTurning;
    public $email;

    public function rules() {

        return [
            [['width', 'size', 'camera', 'flap', 'email'], 'required'],
            [['flap', 'flapTurning'], 'integer'],
            [['width'], 'compare', 'compareValue' => 70, 'operator' => '>=', 'type' => 'number'],
            [['width'], 'compare', 'compareValue' => 210, 'operator' => '<=', 'type' => 'number'],
            [['size'], 'compare', 'compareValue' => 100, 'operator' => '>=', 'type' => 'number'],
            [['size'], 'compare', 'compareValue' => 200, 'operator' => '<=', 'type' => 'number'],
            [['camera'], 'in', 'range' => [self::QUANTITY_1, self::QUANTITY_2, self::QUANTITY_3]],
            [['flap'], 'compare', 'compareValue' => 1, 'operator' => '>=', 'type' => 'number'],
            [['flapTurning'], 'compare', 'compareAttribute' => 'flap', 'operator' => '<='],
            ['email', 'email'],
        ];
    }

    public static function messageRun($model) {



            $result = Yii::$app->mailer->compose('/mailer/list', ['model' => $model])
                    ->setFrom('viktor.testproject.php.up@gmail.com')
                    ->setTo($model['email'])
                    ->setSubject('Тема сообщения')
                    ->send();

            return var_dump($result);
        
    }

}
