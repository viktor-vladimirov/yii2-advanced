<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * Description of Search
 *
 * @author K55VD
 */
class NewsSearch {

    public function simpleSearch($keyword) {

        $sql = "SELECT * FROM news WHERE content LIKE '%$keyword%' LIMIT 20";
        return Yii::$app->db->createCommand($sql)->queryAll();
    }

    public function fulltextSearsh($keyword) {
        
        $sql = "SELECT * FROM news WHERE MATCH (content) AGAINST ('$keyword') LIMIT 20";
        return Yii::$app->db->createCommand($sql)->queryAll();
    }

    public function advancedSearsh($keyword) {
        
        $sql = "SELECT * FROM idx_news_content WHERE MATCH('$keyword') OPTION ranker=WORDCOUNT";
        $data = Yii::$app->sphinx->createCommand($sql)->queryAll();
        
        $ids = ArrayHelper::map($data, 'id', 'id');
 
        $data = News::find()->where(['id' => $ids])->asArray()->all();
        
        $data = ArrayHelper::index($data, 'id');
        
        $result = [];
        foreach ($ids as $element){
            $result[] = [
                'id' => $element,
                'title' => $data[$element]['title'],
                'content' => $data[$element]['content'],
            ];
        }
        return $result;
    }    
}
