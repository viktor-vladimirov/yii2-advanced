<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use frontend\models\Subscribe;

class NewsletterController extends Controller {

    public function actionSubscribe() {


        $formaData = Yii::$app->request->post();
        $model = new Subscribe();

        if (Yii::$app->request->isPost) {
            
            $model->email = $formaData['email'];
            if($model->validate() && $model->save()){
                Yii::$app->session->setFlash('subscriberStatus', 'Subscribe complited !');
                
            }
     
        }

        return $this->render('subscribe',[
            'model' => $model,
        ]);
    }

}
