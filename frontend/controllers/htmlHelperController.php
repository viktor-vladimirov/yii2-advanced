<?php

namespace frontend\controllers;

class HtmlHelperController extends \yii\web\Controller {

    public function actionDemo() {

        return $this->render('demo');
    }

    public function actionEscapeOutput() {

        $comments = [
            [
                'id' => 10,
                'author' => 'Student',
                'text' => 'Hello ! How are you',
            ],
            [
                'id' => 12,
                'author' => 'Vasia',
                'text' => 'Hello ! ',
            ],
            [
                'id' => 12,
                'author' => 'Vasia',
                'text' => '<b>Hello !</b><script>alert("I will steal your money");</script>',
            ],
        ];

        return $this->render('escape-output', [
                    'comments' => $comments,
        ]);
    }

}
