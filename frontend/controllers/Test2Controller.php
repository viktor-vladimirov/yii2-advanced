<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use Faker\Factory;


class Test2Controller extends Controller {

    public function actionGenerate() {

        /* @var $facker  Faker\Generator instance  */


        $faker = Factory::create();

        for ($j = 0; $j < 100; $j++) {

            $news = [];

            for ($i = 0; $i < 100; $i++) {

                $news[] = [$faker->text(35), $faker->text(rand(1000, 2000)), rand(0, 1)];
            }
            Yii::$app->db->createCommand()->batchInsert('news', ['title', 'content', 'status'], $news)->execute();
            unset($news);
        }
        die('stop');

        //return $this->render('index');
    }

}
