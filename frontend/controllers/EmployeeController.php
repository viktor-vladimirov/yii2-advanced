<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use frontend\models\Employee;

class EmployeeController extends Controller {

    public function actionIndex() {

        $employee = new Employee();
        $employee->firstName = 'Alex';
        $employee->lastName = 'Smith';
        $employee->middleName = 'Bob';
        $employee->salary = 1000;


        echo $employee['firstName'];

        echo '<hr>';

        foreach ($employee as $attribute => $value) {
            echo "$attribute: $value <br>";
        }

        echo '<hr>';

        $array = $employee->toArray();
        echo '<pre>';
        print_r($array);
        echo '<pre>';

        echo '<hr>';


        echo '<pre>';
        print_r($employee->getAttributes());
        print_r($employee->attributes);
        echo '<pre>';
    }

    public function actionRegister() {

        $model = new Employee();
        $model->scenario = Employee::SCENARIO_EMPLOYEE_REGISTER;

        if ($model->load(Yii::$app->request->post())) {
            
            if ($model->validate() && $model->save()) {
                Yii::$app->session->setFlash('success','Registred!');
            }
        }


        return $this->render('register', [
                    'model' => $model,
        ]);
    }

    public function actionUpdate() {

        $model = new Employee();
        $model->scenario = Employee::SCENARIO_EMPLOYEE_UPDATE;

        $formdata = Yii::$app->request->post();

        if (Yii::$app->request->isPost) {

            $model->attributes = $formdata;

            if ($model->validate() && $model->save()) {
                Yii::$app->session->setFlash('success','Registred!');
            }
        }
        
        return $this->render('update', [
                    'model' => $model,
        ]);
    }

}
