<?php

namespace console\controllers;

use console\models\News;
use console\models\Subscriber;
use console\models\Sender;
use yii\helpers\Console;


/**
 * Description of MailerController
 *
 * @author K55VD
 */
class MailerController extends \yii\console\Controller {

    public function actionSend() {

        $newsList = News::getList();
        $subscribers = Subscriber::getList();
        $count = Sender::run($subscribers, $newsList);
        
        Console::output("\nEmails sent: {$count}");

    }

}
