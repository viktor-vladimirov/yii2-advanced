<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%employee}}`.
 */
class m200304_100258_create_employee_table extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable('{{%employee}}', [
            'id' => $this->primaryKey(),
            'first_name' => $this->string(255),
            'middle_name' => $this->string(255),
            'last_name' => $this->string(255),
            'birthdate' => $this->date(),
            'city' => $this->string(255),
            'hiring_date' => $this->date(),
            'position' => $this->string(255),
            'departament' => $this->string(255),
            'id_code' => $this->string(255),
            'email' => $this->string()->unique(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropTable('{{%employee}}');
    }

}
