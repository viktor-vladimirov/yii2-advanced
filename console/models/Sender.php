<?php

namespace console\models;

use Yii;

/**

 * @author K55VD
 */
class Sender {

    public static function run($subscribers, $newsList) {

        $viewData = ['newsList' => $newsList];

        $count = 0;

        foreach ($subscribers as $subscriber) {

            $result = Yii::$app->mailer->compose('/mailer/newslist', $viewData)
                    ->setFrom('viktor.testproject.php.up@gmail.com')
                    ->setTo($subscriber['email'])
                    ->setSubject('Тема сообщения')
                    ->send();
            if ($result) {
                $count++;
            }
        }
        return $count;
    }

}
