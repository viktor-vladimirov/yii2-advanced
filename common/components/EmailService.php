<?php

namespace common\components;

use Yii;
use yii\base\Component;
use common\components\UserNotificationInterface;
/**
 * @author K55VD
 */
class EmailService extends Component {

    public function notifyUser(UserNotificationInterface $event) {
        /**
         * @param UserNotificationInterface $user
         * @param string $subject
         * @return bool
         */
        return Yii::$app->mailer->compose()
                        ->setFrom('viktor.testproject.php.up@gmail.com')
                        ->setTo($event->getEmail())
                        ->setSubject($event->getSubject())
                        ->send();
    }
    
    /**
     * 
     * @param UserNotificationInterface $event
     * @return bool
     */
    public function notifyAdmins(UserNotificationInterface $event) {

        
        return Yii::$app->mailer->compose()
                        ->setFrom('viktor.testproject.php.up@gmail.com')
                        ->setTo('scoud@mail.ru')
                        ->setSubject($event->getSubject())
                        ->send();
    }

}
